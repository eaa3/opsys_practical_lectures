#
# Makefile for building our application, making sure to link it with
# the library.
#

# Some useful variables: the directory holding our libraries; and the include
# arguments so gcc can find the include files of libraries when we write:
#   #include <some_library_header.h>
LIBS_DIR = ../libs
INCLUDE_ARGS = -I $(LIBS_DIR)/linked_list/include

# Add the include folders to CFLAGS.
CFLAGS = -Wall -Werror -D _GNU_SOURCE $(INCLUDE_ARGS)
GCC = gcc $(CFLAGS)

# This runs the application if 'all' is first up to date.  Note that, since a
# shared library is loaded and bound at run-time, we must tell the dynamic
# linker where it can find our compiled dynamic library by setting an
# environment variable to the path.  In linux, we can do this inline by
# writing a command:
#  ENV_VAR_NAME=<value> <command_to_run>
run: all
	LD_LIBRARY_PATH=$(LIBS_DIR)/linked_list/src ./todo_list

# For convention's sake.
all: todo_list

# Rule to build the main application, with the following interesting arguments
#  -L <library_dir> -- tells gcc where to look for libraries
#                      whether static or shared.
#  -l<lib_name_minus_lib_prefix> -- tells gcc which library we would like to
#  				    	link against.  Note how the library is
# 					specified without the 'lib' prefix or file
#  					extension of the actual library file.  If
#  					gcc sees a '.so' (shared) library it will prefer it
#  					over a '.a' (static) library.
todo_list: todo_list.o user_interface.o
	$(GCC) todo_list.o user_interface.o -L ../libs/linked_list/src -llinked_list -o $@

todo_list.o: todo_list.c user_interface.h
	$(GCC) -c $< -o $@

user_interface.o: user_interface.c user_interface.h
	$(GCC) -c $< -o $@

clean:
	rm -f *.o todo_list
